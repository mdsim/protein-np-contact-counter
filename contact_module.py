# Description: This module is used to calculate the number of contacts between
#              a nano-particle and protein residues. This script is used as
#              to make some analysis on a simulation of an HSA and a gold 
#              nano-particle during my master thesis in June 2023.
# Input:       The topology and trajectory files of the system.
# Output:      The number of contacts for each frame and the total number of
#              contacts for each residue. 
#              The results are saved in data1.dat and data2.dat, respectively.
#              The names and IDs of residues in contact with NP and the number
#              of contacts are saved in data3.dat.
#              The unique names and IDs of residues in contact with NP and the
#              number of contacts are saved in data4.dat.
#              The plots of the number of contacts for each frame and the
#              histogram of the number of contacts for each residue are saved
#              in data2.png and data4.png, respectively.
# Author:      Oveis Mahmoudi
#              MSc in Physics and Computational Physics
# Email:       oveis.mahmoudi@gmail.com
# Affiliation: Université Bourgogne Franche-Comté, Besançon, France
#              Laboratoire chrono-environnement, UMR CNRS 6249
# Date:        2023-06-15

import MDAnalysis as mda
import numpy as np
import matplotlib.pyplot as plt
from MDAnalysis.lib import distances
from matplotlib.ticker import ScalarFormatter


def initialize(topology, trajectory, protein_selection, nanoparticle_selection):
    '''Initialize the universe, protein and nano-particle.
    
    Parameters
    ----------
    topology : str
        The topology file.
    trajectory : str
        The trajectory file.
    protein_selection : str
        The selection of the protein.
    nanoparticle_selection : str
        The selection of the nano-particle.
         
    Returns
    -------
    universe: The universe object.
    protein: The protein object.
    nano: The nano-particle object.
     
    Notes
    -----
    The selection of the protein and nano-particle is hard-coded.
    '''

    universe = mda.Universe(topology, trajectory)
    protein  = universe.select_atoms(protein_selection)
    nano     = universe.select_atoms(nanoparticle_selection)
    
    print('\nProtein residues: ', protein.residues)
    print('\nNano-particle residues: ', nano.residues)
    return universe, protein, nano



def get_residues_in_contact(protein, nano, dmax=8.0):
    '''Find all residues for which atoms in protein are within the dmax distance of a 
    nano-particle.
     
    Parameters
    ----------
    protein (object): The protein object containing atoms.
    nano (object): The nano-particle object containing atoms.
    dmax (float, optional): The maximum distance threshold for contact (default: 8.0 Angstrom).
        
    Returns
    -------
    residues (object): The residues that have atoms in contact with the nano-particle.
    '''
    
    distance = distances.distance_array(protein.positions, nano.positions, 
               box=protein.universe.trajectory.ts.dimensions)
    atoms_in_contact = np.any(distance <= dmax, axis=1)
    return protein[atoms_in_contact].residues



def contact_counter(u, protein, nano, residue_dictionary, dmax=8.0):
    '''
    Counts the number of residues in contact with nano-particles for each 
    frame.

    Parameters
    ----------
    u (object): The universe object.
    protein (object): The protein object.
    nano (object): The nano-particle object.
    residue_dictionary: The dictionary of residue IDs and names.
    dmax (float, optional): The maximum distance threshold for contact
    (default: 8.0 Angstrom).
    
    Returns
    -------
    res_connected_num (list): The list of the number of residues in contact 
    with nano-particles for each frame.
    '''
    count_total   = np.zeros((u.trajectory.n_frames, 2))   # (time, N_exposed)
    count_residue = np.zeros((u.trajectory.n_frames,
                              len(residue_dictionary.values())+1))

    for i, ts in enumerate(u.trajectory):
        in_contact_residues = get_residues_in_contact(protein, nano, dmax=8.0)
        
        for item in residue_dictionary.keys():
            residue_dictionary[item] = int((in_contact_residues.resids==item).sum())
        
        count_total[i, :]   = (ts.time, in_contact_residues.n_residues)
        count_residue[i, :] = (ts.time, *residue_dictionary.values())

    # Put the results in a list
    res_connected_num = np.zeros(len(residue_dictionary.values()), dtype=int)
    for i in range(len(res_connected_num)):
        res_connected_num[i] =int(sum(count_residue[:, i+1]))

    return res_connected_num



def contact_plot(filename, dmax=8.0):
    '''
    Description
    -----------
    Plot the number of residues in contact with nano-particles for each frame.
    
    Parameters
    ----------
    filename (str): The name of the file containing the contact data.
    dmax (float, optional): The maximum distance threshold for contact
    (default: 8.0 Angstrom).
     
    Returns
    -------
    None
    '''
    data = np.loadtxt(filename)
    plt.figure(figsize=(10, 6))
    plt.title(r"Variation in number of protein residues in contact with nano-particles")
    plt.plot(data[:,0], data[:,1], 
            label=r"$d_\mathrm{{max}}={0:.1f}$ Å".format(dmax))
    plt.xlabel(r"time $t$ (ns)")
    # plt.yticks(np.arange(0, 50, 2))
    plt.ylabel("Protein residue number in contact")
    plt.show()




def uniqize(name, num):
    """
    Description
    -----------
    Sum the values of duplicate entries in the name list and remove them from the num list.
     
    Parameters
    ----------
    name : list
        The list of names.
    num : list
        The list of numbers.
         
    Returns
    -------
    name_new : list
        The new list of names.
    num_new : list
        The new list of numbers.
    """
    if len(name) != len(num):
        raise ValueError("The length of name must be equal to the length of num.")
    
    # New lists to store unique values
    name_new = []
    num_new = []

    # Iterate through the original lists
    for i in range(len(name)):
        # Check if the current name is already in the new list
        if name[i] not in name_new:
            # If it's not a duplicate, add it to the new lists
            name_new.append(name[i])
            num_new.append(num[i])
        else:
            # If it's a duplicate, find the corresponding index in the new lists
            for j in range(len(name_new)):
                if name_new[j] == name[i]:
                    # Sum the values in the num_new list for the duplicate entry
                    num_new[j] += num[i]
    print(name_new)
    print(num_new)
    return name_new, num_new




def histo_from_list(name, num, title='HSA Residues', save=False, output='./Outputs/'+'histo.pdf'):
    '''
    Plots a histogram of the number of residues in contact with nano-particles from a list.
    
    Parameters
    ----------
    name : list
        The list of names.
    num : list
        The list of numbers.
    title : str, optional
        The title of the plot (default: 'HSA Residues').
    save : bool, optional
        If True, save the plot (default: False).
    output : str, optional
        The name of the output file (default: './Outputs/'+'histo.pdf').

    Returns
    -------
    None
    '''
    if len(name) != len(num):
        raise ValueError("The length of name must be equal to the length of num.")

    I = np.arange(1, len(name)+1, 1)
    plt.figure(figsize=(10, 4))
    plt.bar(I, num, color='r', edgecolor='k', alpha=0.65)
    plt.title(title, fontsize=14)
    plt.xlabel('Residue names', fontsize=14)
    plt.ylabel('Number of residues', fontsize=14)
    plt.xticks(I, name, fontsize=10, rotation=45)#, ha='right')
    plt.yticks(fontsize=10)
    for i, v in enumerate(num):
        plt.text(I[i], v + 1, str(v), ha='center', va='bottom', fontsize=10, c='b')

    if save:
        plt.savefig(output, format='pdf', dpi=250)
    plt.show()




def histo_from_file(file_path):
    '''
    Description
    -----------
    Plots a histogram of the number of residues in contact with nano-particles from a file.

    Parameters
    ----------
    file_path : str
        The path of the .dat file containing the data.
    
    Returns
    -------
    None
    '''
    if file_path[-4:] != '.dat':
        raise ValueError("The file must be a .dat file.")
    
    data = np.loadtxt(file_path, dtype=str)
    name_new = data[:, 0]
    num_new = data[:, 1].astype(int)

    arr = np.arange(1, len(name_new)+1, 1)
    plt.figure(figsize=(10, 6))
    plt.bar(arr, num_new, color='b', edgecolor='k', alpha=0.65)
    plt.xlabel('Residue names', fontsize=14)
    plt.ylabel('Number of residues', fontsize=14)
    plt.title('Number of residues in contact with the nano-particle', fontsize=14)
    plt.xticks(arr, name_new, fontsize=14, rotation=45, ha='right')
    plt.yticks(fontsize=14)
    plt.show()


def contact_finder(file):
    '''
    Description
    -----------
    Find the frame of the first contact and the time of the first contact
    in the trajectory.

    Parameters
    ----------
    file : str
        The name of the file containing the contact data.
    
    Returns
    -------
    None
    '''
    data = np.loadtxt(file)
    time = data[:, 0]
    contact = data[:, 1]
    # find the index of the first frame with contact
    for i in range(len(contact)):
        if contact[i] != 0:
            print("frame of first contact: ", i)
            print("Time of first contact: ", time[i])
            break


def insert_average(arr, arr_avg, interval=10):
    '''
    Description
    -----------
    Insert the average value every "interval" values.
    
    Parameters
    ----------
    arr : array
        The array to be modified.
    arr_avg : array
        The array containing the average values.
    interval : int, optional
        The interval of insertion (default: 10).

    Returns
    -------
    arr : array
        The modified array.
    '''
    if len(arr_avg) != len(arr)//interval:
        raise ValueError("The length of arr_avg must be equal to the length of arr divided by interval.")
    
    j = 0
    for i in range(1, len(arr)+1):
        arr[i-1] = arr_avg[j]
        if i%interval == 0:   # Insert the average value every 10 values
            j += 1
            if j == len(arr_avg):
                break
    return arr



def compute_average_per_100(array):
    '''
    Description:
    ------------
    Compute the average of every 100 values in the array
    
    Parameters:
    -----------
    array: numpy array

    Returns:
    --------
    averages: numpy array
    '''
    reshaped_array = array.reshape(-1, 20)  # Reshape the array into groups of 100 values
    averages = np.mean(reshaped_array, axis=1, dtype=int)  # Compute the average along axis=1
    return averages