# Description: This script is used to calculate the number of contacts between
#              a nano-particle and protein residues. This script is used as
#              to make some analysis on a simulation of an HSA and a gold 
#              nano-particle during my master thesis in June 2023.
# Input:       The topology and trajectory files of the system.
# Output:      The number of contacts for each frame and the total number of
#              contacts for each residue. 
#              The results are saved in data1.dat and data2.dat, respectively.
#              The names and IDs of residues in contact with NP and the number
#              of contacts are saved in data3.dat.
#              The unique names and IDs of residues in contact with NP and the
#              number of contacts are saved in data4.dat.
#              The plots of the number of contacts for each frame and the
#              histogram of the number of contacts for each residue are saved
#              in data2.png and data4.png, respectively.
# Author:      Oveis Mahmoudi
#              MSc in Physics and Computational Physics
# Email:       oveis.mahmoudi@gmail.com
# Affiliation: Université Bourgogne Franche-Comté, Besançon, France
#              Laboratoire chrono-environnement, UMR CNRS 6249
# Date:        2023-06-15
 

from contact_module import *
from time import time

###############################################################################
#                             Initiallization                                 #
###############################################################################
t1 = time()
path              = './path_to_the_file/'
tpr_file          = 'centered_prot_np.tpr'
xtc_file          = 'centered_prot_np.xtc'
protein_selection = 'name BB SC1 SC2 SC3'
nano_selection    = 'name MM A0 A1 A2 A3'

# Output files to save the results
data_1 = 'data1.dat'    # total num of contacts for each residue
data_2 = 'data2.dat'    # results for each frame
data_3 = 'data3.dat'    # names and IDs of residues in contact with NP and the
                        # number of contacts
data_4 = 'data4.dat'    # Unique names and IDs of residues in contact with NP 
                        # and the number of contacts

u, protein, nano = initialize(path+tpr_file, path+xtc_file)

resid_ls   = list(protein.atoms.residues.resids)
resname_ls = list(protein.atoms.residues.resnames)
res_dict   = dict(zip(resid_ls, resname_ls))

print("No. of IDs: {} \nNo. of resnames: {}".format(len(resid_ls),
                                                    len(resname_ls)))


###############################################################################
#                             Contact Analysis                                #
###############################################################################
count_total   = np.zeros((u.trajectory.n_frames, 2))   # (time, N_exposed)
count_residue = np.zeros((u.trajectory.n_frames,
                          len(res_dict.values())+1))   # (time, N_exposed)

for i, ts in enumerate(u.trajectory):
    if i%100 == 0:
        print(i)
    
    res_in_contact = get_residues_in_contact(protein, nano, dmax=8.0)
    
    for item in res_dict.keys():
        res_dict[item] = int((res_in_contact.resids==item).sum())
    
    count_total[i, :]   = (ts.time, res_in_contact.n_residues)
    count_residue[i, :] = (ts.time, *res_dict.values())


# Put the results in a list
res_connected_num = np.zeros(len(res_dict.values()), dtype=int)
for i in range(len(res_connected_num)):
    res_connected_num[i] =int(sum(count_residue[:, i+1]))
print(res_connected_num)
print(len(res_connected_num))


# Save the total num of contacts for each residue
with open(path+data_1, 'w') as f:
    f.write('# resid\tN_contacts\n')
    for i in range(len(resid_ls)):
        f.write('{}\t{}\t{}\n'.format(resname_ls[i], resid_ls[i], 
                                      res_connected_num[i]))
print('\ndata1.dat saved in {}.'.format(path))
        
# Save the results for each frame
with open(path+data_2, 'w') as f:
    f.write('# time (ns)\tN_contacts\n')
    for i in range(np.shape(count_total)[0]):
        f.write('{}\t{}\n'.format(count_total[i,0]/1000, count_total[i,1]))
print('\ndata2.dat saved in {}.'.format(path))



# Load the data2.dat file and find the index of the first frame with contact
data = np.loadtxt(path+"data2.dat")
time = data[:, 0]
contact = data[:, 1]
# find the index of the first frame with contact
for i in range(len(contact)):
    if contact[i] != 0:
        print("\n The frame of the first contact: ", i)
        print("\n The time of the first contact: ", time[i])
        break



# Find the index of the residues with non-zero values and give the residue name
# and number
connection_number = []
connected_res_name = []
connected_res_id = []
for i in range(len(res_connected_num)):
    if res_connected_num[i] != 0:
        connection_number.append(res_connected_num[i])
        connected_res_name.append(resname_ls[i])
        connected_res_id.append(resid_ls[i])
print(connection_number)
print(connected_res_name)
print(connected_res_id)



# Find the unique residue names and the number of residues in contact with NP
name_new, num_new = uniqize(connected_res_name, connection_number)
print(name_new)
print(num_new)


# Save the names and IDs of residues in contact with NP and the number of 
# contacts in a file
with open(path+data_3, 'w') as f:
    f.write('# resid\tresname\tN_contacts\n')
    for i in range(len(connected_res_name)):
        f.write('{}\t{}\t{}\n'.format(connected_res_name[i],
                                      connected_res_id[i],
                                      connection_number[i]))
print('\ndata3.dat saved in {}.'.format(path))



# Save the unique names and IDs of residues in contact with NP and the number of
# contacts
with open(path+data_4, 'w') as f:
    f.write('# resid\tresname\tN_contacts\n')
    for i in range(len(name_new)):
        f.write('{}\t{}\n'.format(name_new[i], num_new[i]))
print('\ndata4.dat saved in {}.'.format(path))


###############################################################################
#                             Plotting                                        #
###############################################################################
# Plot: Conntacts vs Time
contact_plot(path+data_2, dmax=8.0)


histo_from_file(path+data_4)

time_elapsed = time() - t1
print('Time elapsed: {:.2f} min'.format(time_elapsed/60))

