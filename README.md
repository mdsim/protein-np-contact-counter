## Protein-nanoparticle contact counter

# Description
This script is developed to calculate the number of contacts between a nano-particle and protein residues. It is used to make some analyses on a simulation of an HSA and a gold nano-particle during my master thesis in June 2023.

# Support
Do not hesitate to contact me if you have any questions, find any mistakes or any possible way of improving the codes.
Email: oveis.mahmoudi@gmail.com

# License
The codes are developed by Oveis Mahmoudi and are free to use and modify.

# Project status
This project is finished in June 15, 2023, as part of my end-of-study internship at Laboratoire chrono-environnement, UMR CNRS 6249, for master's degree in Physics & Computational Physics at Université de Franche-Comté, Besançon, France. 